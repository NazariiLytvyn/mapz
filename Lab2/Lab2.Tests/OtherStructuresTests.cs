﻿using Lab2.BLL.Entities;
using NUnit.Framework;

namespace Lab2.Tests
{
    [TestFixture]
    public class OtherStructuresTests
    {
        private Data _moq;

        [SetUp]
        public void SetUp()
        {
            _moq = new Data();
        }

        [Theory]
        [TestCase("4")]
        [TestCase("w")]
        [TestCase("q")]
        public void OtherStructures_ShouldReturnEmptyCollection(string str)
        {
            var result = OtherStructures.StringQueue(_moq.Task, str);
            Assert.That(result, Is.Empty);
        }

        [Theory]
        [TestCase("t")]
        [TestCase("9")]
        [TestCase("4T")]
        public void OtherStructures_ShouldReturnCollection(string str)
        {
            var result = OtherStructures.StringQueue(_moq.Task, str);
            Assert.That(result, Is.Not.Empty);
        }

    }
}