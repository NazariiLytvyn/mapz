﻿using Lab2.BLL.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Tests
{
    [TestFixture]
    public class DictionaryTests
    {
        private Data _moq;

        [SetUp]
        public void SetUp()
        {
            _moq = new Data();
        }

        [Test]
        public void DictMaker_ShouldReturnCollection()
        {
            var result = DictionaryMaker.ProjectsDictionary(_moq.Projects);
            Assert.That(result, Is.Not.Empty);
        }

        [Theory]
        [TestCase(555)]
        [TestCase(888)]
        [TestCase(777)]
        public void DictMaker_UnexistingIdShouldNotReturn(int id)
        {
            var result = DictionaryMaker.ProjectsDictionary(_moq.Projects);
            Assert.That(!result.ContainsKey(id));
        }
    }
}
