﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public static class DictionaryMaker
    {
       
        public static Dictionary<int, Project> ProjectsDictionary(List<Project> lst)
        {
            var res = lst.ToDictionary(x => x.ID, x => x);
            return res;
        }

        public static Dictionary<int, List<Task>> TasksDictionary(List<Task> lst)
        {
            var ls = lst
                .GroupBy(p => p.ProjectID)
                .OrderBy(g => g.Count())
                .ToDictionary(g => g.Key, g => g.OrderBy(p => p.Name)
                .ToList());
            
            return ls;
        }

    }
}
