﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public class Data
    {
        public List<Project> Projects
        {
            get
            {
                return new List<Project>()
                {
                    new Project("1Project1", "Description", 1),
                    new Project("3Project2", "Description", 2),
                    new Project("1Project3", "Description", 3),
                    new Project("9Project4", "Description", 4),
                    new Project("5Project5", "Description", 5),
                    new Project("7Project6", "Description", 6),
                    new Project("9Project7", "Description", 7)

                };
            }
        }

        public List<Task> Task
        {
            get
            {
                return new List<Task>()
                {
                    new Task(1, "9Task1", "Description1", DateTime.Now, DateTime.Now.AddDays(10), 1),
                    new Task(2, "8Task2", "Description2", DateTime.Now, DateTime.Now.AddDays(25), 1),
                    new Task(3, "tTask3", "Description3", DateTime.Now, DateTime.Now.AddDays(13), 2),
                    new Task(4, "5Task4", "Description4", DateTime.Now, DateTime.Now.AddDays(41), 3),
                    new Task(5, "4Task5", "Description5", DateTime.Now, DateTime.Now.AddDays(26), 3),
                    new Task(6, "tTask6", "Description6", DateTime.Now, DateTime.Now.AddDays(48), 3),
                    new Task(7, "8Task7", "Description7", DateTime.Now, DateTime.Now.AddDays(19), 4),
                    new Task(8, "9Task8", "Description8", DateTime.Now, DateTime.Now.AddDays(76), 5),
                    new Task(9, "0Task9", "Description9", DateTime.Now, DateTime.Now.AddDays(40), 6),
                    new Task(9, "tTask10", "Description10", DateTime.Now, DateTime.Now.AddDays(35), 6)
                };
            }
        }

    }
}
