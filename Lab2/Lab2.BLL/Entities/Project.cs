﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public class Project
    {
        public int ID { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Project(string name, string description, int id)
        {
            ID = id;
            Description = description;
            Name = name;
        }

        public override string? ToString()
        {
            return $"{ID} {Name} {Description}";
        }
    }
}
