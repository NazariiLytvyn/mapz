﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public static class ListExtension
    {
        public static void PrintInConsole<T>(this List<T> lst)
        {
            Console.WriteLine(String.Join(Environment.NewLine, lst));
        }
        public static string GetStringFormat<T>(this List<T> lst)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in lst)
            {
                sb.Append(item.ToString());
                sb.AppendLine(Environment.NewLine);
            }

            return sb.ToString();
        }
    }
}
