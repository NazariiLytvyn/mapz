﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1.Classes
{
    class ModTestClass
    {
        int a = 1;
        private int b = 2;
        protected int c = 3;
        public int d = 4;
        void foo()
        {
           
        }
        public ModTestClass(int a, int b, int c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public ModTestClass()
        {
            this.a = 0;
            this.b = 0;
            this.c = 0;
        }
    }
}
