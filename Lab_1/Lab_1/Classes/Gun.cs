﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1.Classes
{
    public class Gun : Weapon , IShot
    {
        public Gun() : base()
        {
            magazine_size = 5;
            bullets = 5;
        }
        public Gun(int magazine_size) : base()
        {
            this.magazine_size = magazine_size;
            bullets = magazine_size;
        }

        public int magazine_size { get; }
        public int bullets { get; set; }

        protected override bool isLoaded { get; set; }

        public override void Reload(ref Weapon weapon)
        {
            if(weapon is Gun gun)
            {
                Console.WriteLine("Перезарядка!");
                gun.isLoaded = true;
                gun.bullets = gun.magazine_size;
            }
        }

        public override void isLoaded_info()
        {
            if (bullets > 0)
            {
                Console.WriteLine($"Заряджено, кiлькiсть патронiв {this.bullets}");
            }
            else
            {
                Console.WriteLine("Розряджено");
            }
        }

        public void Shot()
        {
            if (bullets > 0)
            {
                Console.WriteLine("пiу!");
                bullets--;
            }
            else
            {
                isLoaded = false;
            }
        }
    }
}
