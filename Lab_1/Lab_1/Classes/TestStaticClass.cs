﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1.Classes
{
    public class TestStaticClass
    {
        public TestStaticClass()
        {
            count++;
        }
        public static int count { get; set; }
        public static void TestFoo()
        {
            int a = 5;
        }
    }
}
