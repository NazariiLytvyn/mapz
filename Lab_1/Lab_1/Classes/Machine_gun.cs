﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1.Classes
{
    public class Machine_gun : Gun
    {

        private static int _count;

        public int Count { get => _count; }

        public string Name { get; set; }

        protected int weight = 3000;

        private int test = 213;

        public Machine_gun(string name) : base(20)
        {
            Name = name;
            _count++;
        }
        static Machine_gun()
        {
            _count = 0;
        }

        public static implicit operator Machine_gun(int x)
        {
            return new Machine_gun(x.ToString());
        }

        public static explicit operator int(Machine_gun machine_gun)
        {
            return machine_gun.Count;
        }

        public void Deconstruct(out string machine_gunName)
        {
            machine_gunName = Name; 
        }

        public override string ToString()
        {
            return $"Цей автомат називається {Name}!";
        }

        public class GunID
        {
            public Guid ID { get; set; }
            public DateTime Created { get; private set; }
            
            public GunID()
            {
                Created = DateTime.Now.AddYears(2);
                ID = Guid.NewGuid();
            }
        }

    }
}
