﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1.Classes
{
    public abstract class Weapon 
    {
        public Weapon()
        {
            isLoaded = true;
        }

        protected abstract bool isLoaded { get; set; }
        public virtual void isLoaded_info()
        {
            if (isLoaded)
            {
                Console.WriteLine("Заряджено");

            }
            else
            {
                Console.WriteLine("Розряджено");
            }
        }
        public abstract void Reload(ref Weapon weapon);
    }
}
