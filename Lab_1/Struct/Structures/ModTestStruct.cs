﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.Structures
{
    struct ModTestStruct
    {
        int a = 1;
        private int b = 2;
        public int c = 3;
        void foo()
        {

        }
        public ModTestStruct(int a, int b, int c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}
