﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.Structures
{
    internal interface IShot
    {
        Сaliber Type { get; }

        public void Shot();
    }
}
