﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.Structures
{
    internal struct EnumDemo
    {
        public Сaliber FirstType { get; set; }
        public Сaliber SecondType { get; set; }

        public EnumDemo(Сaliber t1, Сaliber t2)
        {
            FirstType = t1;
            SecondType = t2;
        }

        public void Demonstrate()
        {
            Console.WriteLine($"Operator & {FirstType & SecondType}");
            Console.WriteLine($"Operator | {FirstType | SecondType}");
            Console.WriteLine($"Operator ^ {FirstType ^ SecondType}");
        }
    }
}
