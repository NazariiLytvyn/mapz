﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.Structures
{
    public struct Machine_gun : IShot
    {

        private static int _count;

        public int Count { get => _count; }

        public string Name { get; set; }

        public Сaliber Type { get; private set; }

        private int bullets { get; set; }

        public  bool isLoaded { get; set; }

        public Machine_gun(string name)
        {
            Name = name;
            bullets = 10;
            isLoaded = true;
            Type = Сaliber.Seven;
            _count++;
        }
    
        static Machine_gun()
        {
            _count = 0;
        }

        public void isLoaded_info()
        {
            if (isLoaded)
            {
                Console.WriteLine("Заряджено");

            }
            else
            {
                Console.WriteLine("Розряджено");
            }
        }

        public static implicit operator Machine_gun(int x)
        {
            return new Machine_gun(x.ToString());
        }

        public static explicit operator int(Machine_gun machine_gun)
        {
            return machine_gun.Count;
        }

        public void Deconstruct(out string machine_gunName)
        {
            machine_gunName = Name;
        }

        public override string ToString()
        {
            return $"Цей автомат називається {Name}!";
        }

        private class GunID
        {
            public Guid ID { get; set; }
            public DateTime Created { get; private set; }

            public GunID()
            {
                Created = DateTime.Now.AddYears(2);
                ID = Guid.NewGuid();
            }
        }

        public void Shot()
        {
            if (bullets > 0)
            {
                Console.WriteLine("пiу!");
                bullets--;
            }
            else
            {
                isLoaded = false;
            }
        }
    }
}
