﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struct.Structures
{   
    internal struct Boxing
    {
        public static void ShowExample(object o1, object o2)
        {
            Console.WriteLine(o1.ToString() + " " + o2.ToString());
            Console.WriteLine("Unboxing");
            var h1 = (Machine_gun)o1;
            var h2 = (Machine_gun)o2;
            h1.isLoaded_info();
            h2.isLoaded_info();
        }

    }
}
