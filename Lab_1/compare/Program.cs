﻿using Lab_1.Classes;
using System.Diagnostics;

long size = 1000000;
Stopwatch stopWatch = new Stopwatch();

List<TestSimpleClass> lst1 = new List<TestSimpleClass>();
List<TestStaticClass> lst2 = new List<TestStaticClass>();

stopWatch.Start();


for (int i = 0; i < size; i++)
{
    lst1.Add(new TestSimpleClass());
}

stopWatch.Stop();

Console.WriteLine("Створення звичайного класу");
Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

for (int i = 0; i < size; i++)
{
    lst2.Add(new TestStaticClass());
}

stopWatch.Stop();

Console.WriteLine("Створення cтатичного класу");
Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();


foreach (TestSimpleClass item in lst1)
{
    item.TestFoo();
}

stopWatch.Stop();

Console.WriteLine("Виклик звичайної функцiї");
Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

for (int i = 0; i < size; i++)
{
    TestStaticClass.TestFoo();
}

stopWatch.Stop();
Console.WriteLine("Виклик статичної функцiї");
Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();


foreach (TestSimpleClass item in lst1)
{
    item.count++;
}

stopWatch.Stop();
Console.WriteLine("Операцiя зi звичайною змiнною");
Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

for (int i = 0; i < size; i++)
{
    TestStaticClass.count++;
}
Console.WriteLine("Операцiя зi статичною змiнною");
Console.WriteLine(stopWatch.ElapsedMilliseconds);
