﻿using Lab_3.Classes;
using Lab_3.Interfaces;


#region abstract_factory_demo
Console.WriteLine("Abstract factory start");
AbstractFactory firstFactory = new FirstItemFactory();
AbstractFactory secondFactory = new SecondItemFactory();
Person p1 = new Person(firstFactory);
Person p2 = new Person(secondFactory);
Console.WriteLine("\nFirst Persone");
Console.WriteLine(p1);
Console.WriteLine("\nSecond Persone");
Console.WriteLine(p2);
Console.WriteLine("Abstract factory end\n\n");
#endregion

#region singleton_demo
Console.WriteLine("Singleton start");
IDirector manager = Director.Initialize();
Console.WriteLine("Singleton end\n\n");
#endregion


#region prototype_demo
Console.WriteLine("prototype start");
ITable circle = new CircleTable(5);
ITable rectangle = new RectangleTable(2, 3);
Table table = new Table(rectangle);
table.ShowPicture();
table.Rework(circle);
table.ShowPicture();
Console.WriteLine("prototype end\n\n");
#endregion