﻿using Lab_3.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Interfaces
{
    public interface IDirector
    {
        public void AddCompany(Company company);
    }
}
