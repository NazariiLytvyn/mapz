﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    public abstract class AbstractFactory
    {
        public abstract AbstractMcDonalds CookBurger();
        public abstract AbstractPizzeria CookPizza();
    }
}
