﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    public class Burger : AbstractMcDonalds
    {
        public override string? ToString()
        {
            return "I am a Burger";
        }
    }
}
