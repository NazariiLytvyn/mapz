﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    public class Person
    {
        private AbstractMcDonalds _McDonalds;
        private AbstractPizzeria _Pizzeria;

        public Person(AbstractFactory factory)
        {
            _McDonalds = factory.CookBurger();
            _Pizzeria = factory.CookPizza();
        }

        public override string? ToString()
        {
            return $"{_McDonalds}\n{_Pizzeria}";
        }
    }
}
