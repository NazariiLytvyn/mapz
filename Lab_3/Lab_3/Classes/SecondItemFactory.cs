﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    public class SecondItemFactory : AbstractFactory
    {
        public override AbstractMcDonalds CookBurger()
        {
            return new Milkіshake();
        }

        public override AbstractPizzeria CookPizza()
        {
            return new Cola();
        }
    }
}
