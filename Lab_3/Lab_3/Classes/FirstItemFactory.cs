﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    public class FirstItemFactory : AbstractFactory
    {
        public override AbstractMcDonalds CookBurger()
        {
            return new Burger();
        }

        public override AbstractPizzeria CookPizza()
        {
            return new Pizza();
        }
    }
}
