﻿using Lab_3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    public class CircleTable : ITable
    {
        private int _radius;
        public CircleTable(int r)
        {
            _radius = r;
        }

        public ITable Clone()
        {
            return new CircleTable(this._radius);
        }

        public void GetInfo()
        {
            Console.WriteLine("Circle with radius {0}", _radius);
        }
    }
}
