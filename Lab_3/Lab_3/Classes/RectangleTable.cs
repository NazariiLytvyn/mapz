﻿using Lab_3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    internal class RectangleTable : ITable
    {
        private int _width;
        private int _height;

        public RectangleTable(int w, int h)
        {
            _width = w;
            _height = h;
        }

        public ITable Clone()
        {
            return new RectangleTable(this._width, this._height);
        }

        public void GetInfo()
        {
            Console.WriteLine("Rectangle height {0} и width {1}", _height, _width);
        }
    }
}
