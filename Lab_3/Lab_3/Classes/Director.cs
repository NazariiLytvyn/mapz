﻿using Lab_3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3.Classes
{
    public class Director : IDirector
    {
        private static Director single = null;

        private List<Company> _companyList;

        public void AddCompany(Company company)
        {
            _companyList.Add(company);
        }

        protected Director()
        {
            Console.WriteLine("Singleton creating");
        }

        public static Director Initialize()
        {
            if (single == null)
            {
                single = new Director();
            }

            return single;
        }
    }
}
